import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt

# Parse the XML file
try:
    tree = ET.parse("4nodes_example.flowmon")
    root = tree.getroot()
except ET.ParseError as e:
    print(f"Error parsing XML file: {e}")
    exit()

# Create a dictionary of flow statistics
flow_stats = {}
for flow in root.iter("Flow"):
    try:
        flow_id = flow.get("flowId")
        packets_sent = int(flow.get("txPackets"))
        packets_received = int(flow.get("rxPackets"))
        bytes_sent = int(flow.get("txBytes"))
        bytes_received = int(flow.get("rxBytes"))
        delay_sum = int(float(flow.get("delaySum").replace("ns", "")))
        jitter_sum = int(float(flow.get("jitterSum").replace("ns", "")))
        flow_stats[flow_id] = {
            "packets_sent": packets_sent,
            "packets_received": packets_received,
            "bytes_sent": bytes_sent,
            "bytes_received": bytes_received,
            "delay_sum": delay_sum,
            "jitter_sum": jitter_sum,
        }
    except (TypeError, ValueError):
        pass
print("Flow stats:\n")

# Print the flow statistics in a table
print("{:<10} {:<15} {:<20} {:<15} {:<15} {:<10} {:<10}".format(
    "Flow ID", "Packets Sent", "Packets Received", "Bytes Sent", "Bytes Received", "Delay Sum", "Jitter Sum"
))
for flow_id, stats in flow_stats.items():
    print("{:<10} {:<15} {:<20} {:<15} {:<15} {:<10} {:<10}".format(
        flow_id,
        stats["packets_sent"],
        stats["packets_received"],
        stats["bytes_sent"],
        stats["bytes_received"],
        stats["delay_sum"],
        stats["jitter_sum"],
    ))
    
print("\n---------------------------------------------------------------------------------------------------------\n")

# Create a dictionary of IP statistics
ip_stats = {}
for flow_2 in root.iter("Flow"):
    try:
        flow_id_2 = flow_2.get("flowId")
        source_address = flow_2.get("sourceAddress")
        destination_address = flow_2.get("destinationAddress")
        protocol = int(flow_2.get("protocol"))
        source_port = int(flow_2.get("sourcePort"))
        destination_port = int(flow_2.get("destinationPort"))
        ip_stats[flow_id_2] = {
            "source_address": source_address,
            "destination_address": destination_address,
            "protocol": protocol,
            "source_port": source_port,
            "destination_port": destination_port,
        }
    except (TypeError, ValueError):
        pass
print("IP stats:\n")

# Print the IP statistics in a table
print("{:<10} {:<15} {:<25} {:<20} {:<15} {:<10}".format(
    "Flow ID","Source Address", "Destination Address", "Protocol", "Source Port", "Destination Port"
))
for flow_id_2, stats in ip_stats.items():
    print("{:<10} {:<15} {:<25} {:<20} {:<15} {:<10}".format(
        flow_id_2,
        stats["source_address"],
        stats["destination_address"],
        stats["protocol"],
        stats["source_port"],
        stats["destination_port"],
    ))


# Create a bar chart of the delay statistics
labels = ["Flow 1", "Flow 2"]
delay_sum = [flow_stats["1"]["delay_sum"], flow_stats["2"]["delay_sum"]]

x = range(len(labels))
width = 0.5
fig, ax = plt.subplots()
rects1 = ax.bar(x, delay_sum, width, label="Delay Sum")

ax.set_ylabel("Delay (ns)")
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()

fig.tight_layout()
plt.show()