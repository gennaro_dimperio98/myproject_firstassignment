#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/udp-client.h"
#include "ns3/udp-server.h"
#include "ns3/flow-monitor-module.h"
#include <fstream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("4nodes_example");

int main(int argc, char* argv[]) {

  // Parse command line arguments
  CommandLine cmd(__FILE__);
  cmd.Parse(argc, argv);

  // Enable logging for UdpClient and UdpServer
  Time::SetResolution(Time::NS);
  LogComponentEnable("UdpClient", LOG_LEVEL_INFO);
  LogComponentEnable("UdpServer", LOG_LEVEL_INFO);

  // Create 4 nodes
  NodeContainer nodes;
  nodes.Create(4);

  // Install Internet stack on the nodes
  InternetStackHelper internet;
  internet.Install(nodes);

  // Create a CSMA network with a data rate of 5 Mbps, a delay of 2 ms, and an MTU of 1400 bytes
  CsmaHelper csma;
  csma.SetChannelAttribute("DataRate", DataRateValue(DataRate(5000000)));
  csma.SetChannelAttribute("Delay", TimeValue(MilliSeconds(2)));
  csma.SetDeviceAttribute("Mtu", UintegerValue(1400));
  NetDeviceContainer devices = csma.Install(nodes);

  // Assign IP addresses to the devices
  Ipv4AddressHelper ipv4;
  ipv4.SetBase("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaces = ipv4.Assign(devices);

  // Install a UDP server on node 0 that listens on port 9
  UdpServerHelper server1(9);
  ApplicationContainer serverApps1 = server1.Install(nodes.Get(0));
  serverApps1.Start(Seconds(0.5));
  serverApps1.Stop(Seconds(10.0));
  
  // Install a UDP server on node 1 that listens on port 10
  UdpServerHelper server2(10);
  ApplicationContainer serverApps2 = server2.Install(nodes.Get(1));
  serverApps2.Start(Seconds(0.5));
  serverApps2.Stop(Seconds(10.0));

  // Set values to attributes of clients
  uint32_t packetSize = 1024;
  uint32_t maxPacketCount = 1;
  Time interPacketInterval = Seconds(0.1);

  // Configure the UDP client on node 2 to send packets to the first network interface of the local network on port 9
  UdpClientHelper client1(interfaces.GetAddress(0), 9);
  client1.SetAttribute("MaxPackets", UintegerValue(maxPacketCount));
  client1.SetAttribute("Interval", TimeValue(interPacketInterval));
  client1.SetAttribute("PacketSize", UintegerValue(packetSize));
  ApplicationContainer clientApps1 = client1.Install(nodes.Get(2));
  clientApps1.Start(Seconds(1.0));
  clientApps1.Stop(Seconds(10.0));

  // Configure the UDP client on node 3 to send packets to the second network interface of the local network on port 10
  UdpClientHelper client2(interfaces.GetAddress(1), 10);
  client2.SetAttribute("MaxPackets", UintegerValue(maxPacketCount));
  client2.SetAttribute("Interval", TimeValue(interPacketInterval));
  client2.SetAttribute("PacketSize", UintegerValue(packetSize));
  ApplicationContainer clientApps2 = client2.Install(nodes.Get(3));
  clientApps2.Start(Seconds(1.0));
  clientApps2.Stop(Seconds(10.0));

  // Enable tracing and packet capture
  AsciiTraceHelper ascii;
  csma.EnableAsciiAll(ascii.CreateFileStream("4nodes_example.tr"));
  csma.EnablePcapAll("realtime-udp", false);

  // Install a flow monitor on all nodes
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();

  // Run the simulation for 11 seconds
  Simulator::Stop(Seconds(11.0));
  Simulator::Run();
  monitor->CheckForLostPackets ();

  // Get flow statistics and write them to a file
  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
  std::cout << "\nAll values of the flow are written to the file '4nodes_example.flowmon'\n";
  monitor->SerializeToXmlFile("4nodes_example.flowmon", true, true);

  // Clean up and exit
  Simulator::Destroy();
  return 0;
}
